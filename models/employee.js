const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
  _first_name: {type: String},
  _last_name: {type: String},
  _email: {type: String},
  _phone_number: {type: String},
  _hire_date: {type: String},
  _job_id: {type: Number},
  _salary: {type: Number},
  _commission_pct: {type: Number},
  _manager_id: {type: Number},
  _department_id: {type:Number}

});

class Employee {
  constructor (first_name,last_name,email,phone_number,hire_date,job_id,salary,commission_pct,manager_id,department_id){
    this._first_name = first_name,
    this._last_name = last_name,
    this._email = email,
    this._phone_number = phone_number,
    this._hire_date = hire_date,
    this._job_id = job_id,
    this._salary = salary,
    this._commission_pct = commission_pct,
    this._manager_id = manager_id,
    this._department_id = department_id
  }

  get first_name(){
    return this.first_name;
  }

  set first_name(v){
    this.first_name = v;
  }

  get last_name(){
    return this.last_name;
  }

  set last_name(v){
    this.last_name = v;
  }

  get email(){
    return this.email;
  }

  set email(v){
    this.email = v;
  }

  get phone_number(){
    return this.phone_number;
  }

  set phone_number(v){
    this.phone_number = v;
  }

  get hire_date(){
    return this.hire_date;
  }

  set hire_date(v){
    this.hire_date = v;
  }

  get job_id(){
    return this.job_id;
  }

  set job_id(v){
    this.job_id = v;
  }

  get salary(){
    return this.salary;
  }

  set salary(v){
    this.salary = v;
  }

  get commission_pct(){
    return this.commission_pct;
  }

  set commission_pct(v){
    this.commission_pct = v;
  }

  get manager_id(){
    return this.manager_id;
  }

  set manager_id(v){
    this.manager_id = v;
  }

  get department_id(){
    return this.department_id;
  }

  set department_id(v){
    this.department_id = v;
  }

}

schema.plugin(mongoosePaginate);
schema.loadClass(Employee);
module.exports = mongoose.model('Employee', schema);