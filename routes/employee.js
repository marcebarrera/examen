const express = require('express');
const router = express.Router();
const empCtrl = require('../controllers/employee')

/* GET users listing. */

router.post('/',empCtrl.create);

router.get('/:page?', empCtrl.list);

router.get('/id/:id', empCtrl.index);

router.patch('/:id', empCtrl.update);

router.delete('/:id', empCtrl.destroy);

module.exports = router;