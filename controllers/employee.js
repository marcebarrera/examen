const async = require('async');
const bcrypt = require('bcrypt');
const Employee = require('../models/employee');


function create(req, res) {
  console.log(req.body);
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  let email = req.body.email;
  let phone = req.body.phone;
  let hireDate = req.body.hireDate;
  let jobId = req.body.jobId;
  let salary = req.body.salary;
  let commissionPct = req.body.commissionPct;
  let managerId = req.body.managerId;
  let dptId = req.body.dptId;

  let employee = new Employee({
    _first_name : firstName,
    _last_name : lastName,
    _email : email,
    _phone_number : phone,
    _hire_date : hireDate,
    _job_id : jobId,
    _salary: salary,
    _commission_pct: commissionPct,
    _manager_id: managerId,
    _department_id: dptId

  });

  employee.save().then(obj => res.status(200).json({
    message : employee.create.ok,
    objs : employee
  })).catch(err=> res.status(500).json({
    message : employee.create.err,
    objs : err
  }));

}


function list(req, res, next) {
  const page = req.params.page ? req.params.page : 1;
  Employee.paginate({}, {
    page: page,
    limit: 100
  }).then(employee => res.status(200).json({
    message: res.__('employee.findAll.ok'),
    objs: employee
  })).catch(error => res.status(500).json({
    message: res.__('employee.findAll.err'),
    obj: error
  }));
}

function index(req, res) {
  let id = req.params.id;
  Employee.findOne({ _id: id }).then(employee => res.status(200).json({
    message: res.__('employee.findOne.ok'),
    objs: employee
  })).catch(error => res.status(500).json({
    message: res.__('employee.findOne.err'),
    obj: error
  }));
}

function update(req, res) {
  let id = req.params.id;

  let employee = new Object();

  if (req.body.firstName)
    employee._first_name = req.body.firstName;
  if (req.body.lastName)
  employee._last_name = req.body.lastName;
  if (req.body.email)
  employee._email = req.body.email;
  if (req.body.phoneNumber)
  employee._phone_number = req.body.phoneNumber;
  if (req.body.hire_date)
  employee._hire_date = req.body.hire_date;
  if (req.body.jobId)
  employee._job_id = req.body.jobId;
    if (req.body.salary)
    employee._salary  = req.body.salary;
  if (req.body.commissionPct)
  employee._commission_pct = req.body.commissionPct;
  if (req.body.managerId)
  employee._manager_id = req.body.managerId;
    if (req.body.departmentId)
    employee._department_id = req.body.departmentId;

    Employee.findOneAndUpdate({ _id: id }, employee).then(employee => res.status(200).json({
    message: res.__('employee.update.ok'),
    objs: employee
  })).catch(error => res.status(500).json({
    message: res.__('employee.update.err'),
    obj: error
  }));
}

function destroy(req, res) {
  const id = req.params.id;
  Employee.deleteOne({ _id: id }).then(membemployeee => res.status(200).json({
    message: res.__('employee.delete.ok'),
    objs: employee
  })).catch(error => res.status(500).json({
    message: res.__('employee.delete.err'),
    obj: error
  }));
}

module.exports = {
  create,
  list,
  index,
  update,
  destroy
}